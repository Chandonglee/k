#include"a.h" // ngn/k, (c) 2019-2020 ngn, GNU AGPLv3 - https://bitbucket.org/ngn/k/raw/master/LICENSE
A rsh_(Ax/*0*/,L m,L*pj,L*s,L r)_(Ln=absL(*s);P(r>1,Ay=aA(n);F(n,yai=rsh_(x,m,pj,s+1,r-1))y)
 Ct=tT(xt);Y(t&&!xn,x=cn[t])Lj=*s>=0?*pj:(m-n%m)%m,w=ZT[t],q=min(m-j,n);*pj=(j+n)%m;Ay=atn(t,n);mc(yc,xc+j*w,q*w);mc(yc+q*w,xc,min(j,n-q)*w);
 W(2*m<=n,mc(yc+m*w,yc,m*w);m*=2)Y(n>m,mc(yc+m*w,yc,(n-m)*w))Y(t==tA,y=sqz(mRa(y)))y)
A2(flt,er(!ytT,x,y)Au=atn(yt,0);F(yn,Az=get(y,i);z=app(x,&z,1);Y(!z,ur;u=0;B)z=gL(z);P(!z,xr;yr;ur;0)Fj(gl(z),u=apd(u,get(y,i))))xr;yr;u)
A2(rsh,P(fun(x),flt(x,y))P(ytm,P(xtl,Az=gkv(&y);xR;y=N2(x,z,rsh(x,y));z=N1(y,rsh(x,z));am(y,z))x=enla(x);xR;am(x,ap1(y,x)))
 P(ytM,y=mut(y);yy=ear(cv('#'),A(x,yy),2);y)
 y=!ytT?enl(y):!yn?enl(fir(y)):y;x=enla(Ny(gL(x)));P(!xn,xr;fir(y))
 F(xn,Lv=xli;Y(v<0,ed(xn>2||xn==2&&v-NL||xn==1&&v==NL,x,y)
  P(xn==2,ed(*xl<=0&&xl[1]<=0||!yn,x,y)*xl==NL?K("{x@:1;(x*!1+(-x)!-1+#y)_y}",x,y):K("{x@:0;((-x)!(#y)*!x)_y}",x,y))))
 Lj=0;Au=rsh_(y,yn,&j,xl,xn);xr;yr;u)
A1(enl,P(sim(x),P(pkd(x),Au=atn(tT(xt),1);*ul=x;u)AT(tT(xt),x))P(xtm,Ay=gkv(&x);aM(x,ea1(enl,y)))a1(x))A1(enla,xtt||xtm?enl(x):x)
A2(cat,P(xtmM&&ytmM,P(xtm&&ytm,Az=gkv(&y);amd(A(x,y,av0,z),4))ed(!mtc_(xx,yx),x,y)Au=eac(cv(','),A(mR(xy),mR(yy)),2);Y(u,u=aM(mR(xx),u))xr;yr;u)
 x=enla(x);y=enla(y);Y(!xn,SWP(x,y))P(!yn,yr;x)Y(xt-yt,x=Ny(blw(x));y=Nx(blw(y)))L w=ZT[xt],m=xn,n=yn;
 Au;Y(Ar(x)==1&&ZA+(m+n)*w<=1<<xb,u=AN(m+n,x))E(u=atn(xt,m+n);mc(uc,xc,m*w);Y(xtA,Y(Ar(x)==1,mr(AN(0,x)))E(mRa(x);xr))E(xr))
 mc(uc+m*w,yc,n*w);Y(ytA,mRa(y))yr;u)
A apv(Ax,OV*v)_(asrt(xtT);Ln=xn,w=ZT[xt];
 Y(Ar(x)==1&&ZA+w+n*w<=1ll<<xb,x=AN(n+1,x))E(Ay=atn(xt,n+1);Y(Ar(x)>1&&xtA,mRa(x))EY(ref(x),x=AT(tL,x))m2(x,mc(yc,xc,n*w));x=y)
 V*p=xc+n*w;mc(p,v,w);x)
A2(apd,asrt(xtT||xtM);P(xtM,P(!ytm||!mtc_(xx,yx),apd(Ny(blw(x)),y))x=mut(x);Az=xy=mut(xy);F(An(z),zai=apd(zai,get(yy,i)))yr;x)
 Y(!xtA&&(!ytt||xt-tT(yt)),x=blw(x))P(!xn,xr;enl(y))Lv=xtA?(L)y:gl(y);apv(x,&v))
A apc(Ax,Cc)_(asrt(xtC);x=room(x,1);xc[xn]=c;AN(xn+1,x))A catc(Ax,OC*s,Ln)_(asrt(xtC);x=room(x,n);mc(xc+xn,s,n);AN(xn+n,x))
